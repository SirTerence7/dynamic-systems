#Dynamic_Systems
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backend_bases import MouseButton

class Dynamic():
    def __init__(self, System) -> None:
        cur_xlim = ax1.get_xlim()
        cur_ylim = ax1.get_ylim()
        self.x_range = (cur_xlim[1] - cur_xlim[0])*0.5
        self.y_range = (cur_ylim[1] - cur_ylim[0])*0.5

        self.last_x, self.last_y, self.last_a, self.last_b, self.last_c, self.last_d = 0,0,0,0,0,0
        self.System = System
        self.xy = System.Iterate()
        self.systemplot = ax1.scatter(self.xy[:,0],self.xy[:,1],0.1,c="black")
        Par1, Par2 = self.System.get_Param()
        self.paramplot = ax1.scatter(Par1, Par2, 2, c="g", alpha=0.75)
        self.run = 0
        self.timer_run = False
        self.Point_Plot = ax1.scatter(self.System.x, self.System.y, 10, c="b", alpha=1)

        fig.canvas.mpl_connect('motion_notify_event', self.motion_notify_callback)
        fig.canvas.mpl_connect('button_press_event', self.mouse_press)
        fig.canvas.mpl_connect('key_press_event', self.key_press)
        fig.canvas.mpl_connect('scroll_event', self.mouse_scroll)

    def key_press(self, event) -> None:
        if event.inaxes != ax1:
            return
        if event.key == "r":
            self.System.reset()
        elif event.key == "T":
            self.System.print_Param()
        elif event.key == " ":
            self.timer_run = not self.timer_run
            if self.timer_run:
                self.timer()
        else:
            return
        self.update()

    def mouse_scroll(self, event) -> None:
        if event.inaxes != ax1:
            return
        x_d, y_d = event.xdata, event.ydata
        if x_d == None or y_d == None:
            return
        base_scale = 0.9
        if event.button == "up":
            scale = base_scale
        elif event.button == "down":
            scale = 1/base_scale
        else:
            return

        cur_xlim = ax1.get_xlim()
        cur_ylim = ax1.get_ylim()
        self.x_range *= scale
        self.y_range *= scale
        to_x = x_d+((cur_xlim[1] + cur_xlim[0])*0.5-x_d)*scale
        to_y = y_d+((cur_ylim[1] + cur_ylim[0])*0.5-y_d)*scale
        ax1.set_xlim([to_x - self.x_range, to_x + self.x_range])
        ax1.set_ylim([to_y - self.y_range, to_y + self.y_range])
        fig.canvas.draw_idle()

    def mouse_press(self, event) -> None:
        if event.inaxes != ax1:
            return
        if event.xdata == None or event.ydata == None:
            return
        
        if event.button is MouseButton.MIDDLE:
            self.last_x = event.xdata-self.System.x/self.x_range
            self.last_y = event.ydata-self.System.y/self.y_range
        elif event.button is MouseButton.LEFT:
            self.last_a = event.xdata-self.System.move_factor*self.System.a/self.x_range
            self.last_b = event.ydata-self.System.move_factor*self.System.b/self.y_range
        elif event.button is MouseButton.RIGHT:
            self.last_c = event.xdata-self.System.move_factor*self.System.c/self.x_range
            self.last_d = event.ydata-self.System.move_factor*self.System.d/self.y_range

    def motion_notify_callback(self, event) -> None:
        if event.inaxes != ax1:
            return
        if event.xdata == None or event.ydata == None:
            return

        RET = True
        NOT_JUST_XY = False
        if event.button is MouseButton.MIDDLE:
            self.System.x = ((event.xdata-self.last_x)/(self.x_range))%2*np.pi
            self.System.y = ((event.ydata-self.last_y)/(self.y_range))%2*np.pi
            RET = False
        if event.button is MouseButton.LEFT:
            self.System.a = (event.xdata-self.last_a)/self.System.move_factor*self.x_range
            self.System.b = (event.ydata-self.last_b)/self.System.move_factor*self.y_range
            RET = False
            NOT_JUST_XY = True
        if event.button is MouseButton.RIGHT:
            self.System.c = (event.xdata-self.last_c)/self.System.move_factor*self.x_range
            self.System.d = (event.ydata-self.last_d)/self.System.move_factor*self.y_range
            RET = False
            NOT_JUST_XY = True
        if RET: 
            return
        self.update(NOT_JUST_XY)

    def timer(self) -> None:
        self.run = (self.run+1)%self.System.Iteration
        self.Point_Plot.set_offsets(np.c_[self.xy[self.run,0],self.xy[self.run,1]])
        if self.timer_run:
            pass
            #self.timer()
    
    def update(self, CHANGE:bool = True) -> None:
        if CHANGE:
            self.xy = self.System.Iterate()
        else:
            self.xy = Get_Plane(self.System.xy_list, self.System.x, self.System.y)
        self.systemplot.set_offsets(np.c_[self.xy[:,0],self.xy[:,1]])
        self.paramplot.set_offsets(np.c_[self.System.get_Param()])
        self.Point_Plot.set_offsets(np.c_[self.xy[self.run,0],self.xy[self.run,1]])
        fig.canvas.draw_idle()

class Empty_Class():
    def __init__(self, x:float,y:float, a:float = 1, b:float = -1, c:float = 1, d:float = 1, Iteration: int = 10000) -> None:
        self.Iteration = Iteration
        self.x, self.y, self.a, self.b, self.c ,self.d = x, y, a, b, c ,d
        self.default = x, y, a, b, c ,d
        self.xy_list = np.zeros((self.Iteration,2), dtype=float)
        self.move_factor = 5

    def get_Param(self) -> list:
        return ([self.x,self.a,self.c],[self.y,self.b,self.d])

    def print_Param(self) -> None:
        print("x: ",self.x," y: ",self.y,"a: ",self.a,"b: ",self.b,"c: ",self.c,"d: ",self.d)

    def reset(self) -> list:
        return (self.default)

    def Iterate(self) -> np.ndarray:
        self.xy_list = np.zeros((self.Iteration,2), dtype=float)
        a, b, c ,d = self.a, self.b, self.c ,self.d
        self.xy_list[0,0] = self.x
        self.xy_list[0,1] = self.y
        for i in range(1,self.Iteration):
            x, y = self.xy_list[i-1,0], self.xy_list[i-1,1]
            if abs(x) > 25:
                x = 1/x
            if abs(y) > 25:
                y = 1/y
            self.xy_list[i,0] = (a*(x**2-1)**2-b*(y**2-1)**2+c)
            self.xy_list[i,1] = x**2*y-x*y**2+d

            #paper
            #x: -0.7, y: -0.6, a:-1, b:-0.75, c:1.3, d:1.15
            #(x**3-y**4+a*x+b*y)**(1)
            #(2*x*y)**(1)+c*x+d*y

            #Death of a Star
            #(x**3-y**4+a*x+b*y)**(1)
            #abs(2*x*y)**(1)+c*x+d*y
        return self.xy_list

class Tinkerbell():
    def __init__(self, x:float,y:float, a:float = 0.9, b:float = -0.6, c:float = 2, d:float = 0.5, Iteration: int = 10000) -> None:
        self.Iteration = Iteration
        self.x, self.y, self.a, self.b, self.c ,self.d = x, y, a, b, c ,d
        self.default = x, y, a, b, c ,d
        self.xy_list = np.zeros((self.Iteration,2), dtype=float)
        self.move_factor = 10

    def get_Param(self) -> list:
        return ([self.x,self.a,self.c],[self.y,self.b,self.d])

    def reset(self) -> list:
        return (self.default)

    def Iterate(self) -> np.ndarray:
        self.xy_list = np.zeros((self.Iteration,2), dtype=float)
        a, b, c ,d = self.a, self.b, self.c ,self.d
        self.xy_list[0,0] = self.x
        self.xy_list[0,1] = self.y
        for i in range(1,self.Iteration):
            x, y = self.xy_list[i-1,0], self.xy_list[i-1,1]
            if abs(x) > 3 or abs(y) > 3:
                break
            self.xy_list[i,0] = x**3-y**3+a*x+b*y
            self.xy_list[i,1] = 2*x*y+c*x+d*y
        return self.xy_list

class Henon():
    def __init__(self, x:float = 0, y:float = 0, a:float = 1.4, b:float = 0.3, c:float = 1, d:float = 1, Iteration: int = 10000) -> None:
        self.Iteration = Iteration
        self.x, self.y, self.a, self.b, self.c ,self.d = x, y, a, b, c ,d
        self.default = [x, y, a, b, c ,d]
        self.xy_list = np.zeros((self.Iteration, 2), dtype=float)
        self.move_factor = 10

    def get_Param(self) -> list:
        return ([self.x,self.a,self.c],[self.y,self.b,self.d])
    
    def print_Param(self) -> None:
        print("x: ",self.x," y: ",self.y,"a: ",self.a,"b: ",self.b,"c: ",self.c,"d: ",self.d)

    def reset(self) -> None:
        self.x, self.y, self.a, self.b, self.c ,self.d = self.default

    def Iterate(self) -> np.ndarray:
        self.xy_list = np.zeros((self.Iteration,2), dtype=float)
        a, b, c ,d = self.a, self.b, self.c ,self.d
        self.xy_list[0,0] = self.x
        self.xy_list[0,1] = self.y
        for i in range(1, self.Iteration):
            x, y = self.xy_list[i-1,0], self.xy_list[i-1,1]
            if abs(x) > 3 or abs(y) > 3:
                break
            self.xy_list[i,0] = d-a*x**2+c*y
            self.xy_list[i,1] = b*x
        return self.xy_list

class Lorenz():
    def __init__(self, x:float = 0, y:float = 1, a:float = 10, b:float = 28, c:float = 8/3, d:float = 0.001, Iteration: int = 10000) -> None:
        self.Iteration = Iteration
        self.x, self.y, self.a, self.b, self.c ,self.d = x, y, a, b, c ,d
        self.default = [x, y, a, b, c ,d]
        self.xy_list = np.zeros((self.Iteration, 3), dtype=float)
        self.move_factor = 200

    def get_Param(self) -> list:
        return ([self.x,self.a,self.c],[self.y,self.b,self.d])
    
    def print_Param(self) -> None:
        print("x: ",self.x," y: ",self.y,"a: ",self.a,"b: ",self.b,"c: ",self.c,"d: ",self.d)

    def reset(self) -> None:
        self.x, self.y, self.a, self.b, self.c ,self.d = self.default

    def Iterate(self) -> np.ndarray:
        self.xy_list = np.zeros((self.Iteration,3), dtype=float)
        a, b, c ,d = self.a, self.b, self.c ,self.d
        self.xy_list[0,0] = self.x
        self.xy_list[0,1] = self.y
        self.xy_list[0,2] = 1.05
        for i in range(1, self.Iteration):
            x, y, z = self.xy_list[i-1,0], self.xy_list[i-1,1], self.xy_list[i-1,2]
            if abs(x) > 300 or abs(y) > 300 or abs(z) > 300:
                break
            self.xy_list[i,0] = x + (a*(y-x))*d
            self.xy_list[i,1] = y + (x*(b-z)-y)*d
            self.xy_list[i,2] = z + (x*y-c*z)*d
        #print(self.xy_list[:,:2].shape)
        return Get_Plane(self.xy_list, self.x, self.y)

class Thomas():
    def __init__(self, x:float = 0, y:float = 1, a:float = 10, b:float = 28, c:float = 8/3, d:float = 0.001, Iteration: int = 10000) -> None:
        self.Iteration = Iteration
        self.x, self.y, self.a, self.b, self.c ,self.d = x, y, a, b, c ,d
        self.default = [x, y, a, b, c ,d]
        self.xy_list = np.zeros((self.Iteration, 3), dtype=float)
        self.move_factor = 200

    def get_Param(self) -> list:
        return ([self.x,self.a,self.c],[self.y,self.b,self.d])
    
    def print_Param(self) -> None:
        print("x: ",self.x," y: ",self.y,"a: ",self.a,"b: ",self.b,"c: ",self.c,"d: ",self.d)

    def reset(self) -> None:
        self.x, self.y, self.a, self.b, self.c ,self.d = self.default

    def Iterate(self) -> np.ndarray:
        self.xy_list = np.zeros((self.Iteration,3), dtype=float)
        a, b, c ,d = self.a, self.b, self.c ,self.d
        self.xy_list[0,0] = 0.5
        self.xy_list[0,1] = -0.5
        self.xy_list[0,2] = 1.5
        for i in range(1, self.Iteration):
            x, y, z = self.xy_list[i-1,0], self.xy_list[i-1,1], self.xy_list[i-1,2]
            if abs(x) > 300 or abs(y) > 300 or abs(z) > 300:
                break
            self.xy_list[i,0] = x + (np.sin(y)-a*x)*d
            self.xy_list[i,1] = y + (np.sin(z)-b*y)*d
            self.xy_list[i,2] = z + (np.sin(x)-c*z)*d
        #print(self.xy_list[:,:2].shape)
        return Get_Plane(self.xy_list, self.x, self.y)
    
class Lu_Chen():
    def __init__(self, x:float = 0, y:float = 0, a:float = 36, b:float = 3, c:float = 20, d:float = -15.15, Iteration: int = 10000) -> None:
        self.Iteration = Iteration
        self.x, self.y, self.a, self.b, self.c ,self.d = x, y, a, b, c ,d
        self.default = [x, y, a, b, c ,d]
        self.xy_list = np.zeros((self.Iteration, 3), dtype=float)
        self.move_factor = 200

    def get_Param(self) -> list:
        return ([self.x,self.a,self.c],[self.y,self.b,self.d])
    
    def print_Param(self) -> None:
        print("x: ",self.x," y: ",self.y,"a: ",self.a,"b: ",self.b,"c: ",self.c,"d: ",self.d)

    def reset(self) -> None:
        self.x, self.y, self.a, self.b, self.c ,self.d = self.default

    def Iterate(self) -> np.ndarray:
        self.xy_list = np.zeros((self.Iteration,3), dtype=float)
        a, b, c ,d = self.a, self.b, self.c ,self.d
        self.xy_list[0,0] = 0.1
        self.xy_list[0,1] = 0.3
        self.xy_list[0,2] = -0.6
        for i in range(1, self.Iteration):
            x, y, z = self.xy_list[i-1,0], self.xy_list[i-1,1], self.xy_list[i-1,2]
            if abs(x) > 300 or abs(y) > 300 or abs(z) > 300:
                break
            self.xy_list[i,0] = x + a*(y-x)*0.001
            self.xy_list[i,1] = y + (x-x*z+c*y+d)*0.001
            self.xy_list[i,2] = z + (x*y-b*z)*0.001
        #print(self.xy_list[:,:2].shape)
        return Get_Plane(self.xy_list, self.x, self.y)
    
class Rabinovich_Frabrikant():
    def __init__(self, x:float = 0, y:float = 0, a:float = 0.1, b:float = 0.1, c:float = 0.05, d:float = 0.001, Iteration: int = 10000) -> None:
        self.Iteration = Iteration
        self.x, self.y, self.a, self.b, self.c ,self.d = x, y, a, b, c ,d
        self.default = [x, y, a, b, c ,d]
        self.xy_list = np.zeros((self.Iteration, 3), dtype=float)
        self.move_factor = 200

    def get_Param(self) -> list:
        return ([self.x,self.a,self.c],[self.y,self.b,self.d])
    
    def print_Param(self) -> None:
        print("x: ",self.x," y: ",self.y,"a: ",self.a,"b: ",self.b,"c: ",self.c,"d: ",self.d)

    def reset(self) -> None:
        self.x, self.y, self.a, self.b, self.c ,self.d = self.default

    def Iterate(self) -> np.ndarray:
        self.xy_list = np.zeros((self.Iteration,3), dtype=float)
        a, b, c ,d = self.a, self.b, self.c ,self.d
        self.xy_list[0,0] = 0.1
        self.xy_list[0,1] = -0.1
        self.xy_list[0,2] = 0.1
        for i in range(1, self.Iteration):
            x, y, z = self.xy_list[i-1,0], self.xy_list[i-1,1], self.xy_list[i-1,2]
            if abs(x) > 300 or abs(y) > 300 or abs(z) > 300:
                break
            self.xy_list[i,0] = x + (y*(z-1+x**2)+a*x)*d
            self.xy_list[i,1] = y + (x*(3*z+1-x**2)+b*y)*d
            self.xy_list[i,2] = z + (-2*z*(c+x*y))*d
        #print(self.xy_list[:,:2].shape)
        return Get_Plane(self.xy_list, self.x, self.y)
    
class Fairy():
    def __init__(self, x:float = 0, y:float = 0, a:float = 1, b:float = 1, c:float = 1, d:float = 0.1, Iteration: int = 10000) -> None:
        self.Iteration = Iteration
        self.x, self.y, self.a, self.b, self.c ,self.d = x, y, a, b, c ,d
        self.default = [x, y, a, b, c ,d]
        self.xy_list = np.zeros((self.Iteration, 3), dtype=float)
        self.move_factor = 200

    def get_Param(self) -> list:
        return ([self.x,self.a,self.c],[self.y,self.b,self.d])
    
    def print_Param(self) -> None:
        print("x: ",self.x," y: ",self.y,"a: ",self.a,"b: ",self.b,"c: ",self.c,"d: ",self.d)

    def reset(self) -> None:
        self.x, self.y, self.a, self.b, self.c ,self.d = self.default

    def Iterate(self) -> np.ndarray:
        self.xy_list = np.zeros((self.Iteration,3), dtype=float)
        a, b, c ,d = self.a, self.b, self.c ,self.d
        self.xy_list[0,0] = -0.72
        self.xy_list[0,1] = -0.64
        self.xy_list[0,2] = .72*.64
        for i in range(1, self.Iteration):
            x, y, z = self.xy_list[i-1,0], self.xy_list[i-1,1], self.xy_list[i-1,2]
            if abs(x) > 1000:
                x = 1/x
            if abs(y) > 1000:
                y = 1/y
            if abs(z) > 1000:
                z = 1/z
            self.xy_list[i,0] = x+(-a*x-b*4*(y+z)-c*y**2)*d/100
            self.xy_list[i,1] = y+(-a*x-b*4*(y+x)-c*z**2)*d/100
            self.xy_list[i,2] = z+(-a*x-b*4*(y+x)-c*x**2)*d/100
        #print(self.xy_list[:,:2].shape)
        return Get_Plane(self.xy_list, self.x, self.y)
    
def Get_Plane(xyz:np.ndarray, alpha:float, beta:float) -> np.ndarray:
    '''rotates the xy '''
    alpha = alpha%2*np.pi
    beta = beta%2*np.pi
    sin_a = np.sin(alpha)
    sin_b = np.sin(beta)
    cos_a = np.cos(alpha)
    cos_b = np.cos(beta)

    xy = np.zeros_like(xyz)
    #only neccessary if Get_Plane used wrong, In_place possible and faster

    xy[:,0], xy[:,2] = xyz[:,0]*cos_a-xyz[:,2]*sin_a, xyz[:,0]*sin_a+xyz[:,2]*cos_a
    xy[:,2], xy[:,1] = xyz[:,2]*cos_b-xyz[:,1]*sin_b, xyz[:,2]*sin_b+xyz[:,1]*cos_b
    return xy[:,:2]

fig = plt.figure(figsize=(11, 11))  # instantiate a figure to draw
ax1 = plt.axes()  # create an axes object

#xy = Dynamic(Tinkerbell(-0.7,-0.6,0.9,-0.6,2,0.5,20000))
#xy = Dynamic(Thomas(1,0,0.1998,0.1998,0.1998,0.01,25000))
#xy = Dynamic(Henon(-0.7,-0.6, Iteration = 20000))
#xy = Dynamic(Lorenz(x=0, y=0, Iteration=20000))
xy = Dynamic(Fairy(Iteration=20000))

ax1.set_xlim(-2.5,2.5)
ax1.set_ylim(-2.5,2.5)


plt.show()